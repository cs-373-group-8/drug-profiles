from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def home():
    return render_template('home.html')

@app.route('/drugs')
def drugs():
    return render_template('drugs.html')

@app.route('/conditions')
def conditions():
    return render_template('conditions.html')

@app.route('/side_effects')
def side_effects():
    return render_template('side_effects.html')


